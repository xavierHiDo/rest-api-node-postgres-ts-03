import express, { json } from 'express';
const app = express();

import indexRoutes from './routes/index'

// Middlewares
app.use(json());

app.use(indexRoutes);

app.listen(3000, () =>{
  console.log(`Escuchando en el puerto: ${ 3000 }`);
})
