//CREAR UN PACKAGE.JSON
    npm init -y

//CREAR UN PACKAGE.JSON PAEA TS -> Configurar archivo
    npx tsc --init

//INSTALAR EXPRESS
    npm install express

//INSTALAR PG -> Driver de conexion a postgres
    npm install pg

//INSTALAR TIPOS DE DATOS -> Tipos de datos de express para typescript
    npm install @types/express -D
    npm install @types/pg -D

//INSTALAR CONCURRENTLY -> Ejecutar multiples comandos en un solo comando
    npm install concurrently -D
    npm run dev

//TRANSPILAR EL CODIGO
  npx tsc
  npm run build
